Perlu diingat! pastikan perangkat (Laptop/Komputer) anda terhubung ke internet.

1. Install Server Side Javascript (Node.js) lihat tutorialnya di https://kelasprogrammer.com/cara-install-node-js-di-windows/
2. Install Database (MongoDB) lihat tutorialnya di https://medium.com/@iqbal.abuniyaz/instalasi-mongodb-di-windows-e3626e66d449
3. Jalankan server MongoDB dengan mengetik "mongod" di search Windows
4. Membuat Database baru
    - buka MongoDB Compass lalu klik "CONNECT"
    - klik "CREATE DATABASE"
    - klik "ADD DATA"
    - pilih VIEW "{}"
    - lalu masukkan data dengan copas object berikut ini { "_id" : ObjectId("5f4314a6adaa1a16a87bcb2a"), "username" : "admin", "password" : "admin" }
    - klik "INSERT"
5. Install library Backend, buka CMD lalu ketikkan :
    - cd ..
    - cd ..
    - E:
    - cd apk-karyawan
    - cd server
    - npm i
    - npm start
6. Install library Frontend, buka CMD lalu ketikkan :
    - cd ..
    - cd ..
    - E:
    - cd apk-karyawan
    - cd client
    - npm i
    - npm start
6. Untuk me-running Backend (server) ketikkan di CMD :
    - npm start
7. Untuk me-running Frontend (client) ketikkan di CMD :
    - npm start
8. Buka browser lalu ketikkan "localhost:4200" lalu login dengan memasukkan Username = admin dan Password = admin