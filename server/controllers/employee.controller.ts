import { Express, Request, Response } from 'express';
import { RepositoryService } from './../services/repository.service';
import * as _ from 'lodash';

export class EmployeeController{
    public static setupRoute(app: Express){
        app.get('/api/employee', this.employeeList);
        app.get('/api/employee/:id', this.employeeDetail);
        app.post('/api/employee', this.createEmployee);
        app.put('/api/employee/:id', this.updateEmployee);
        app.post('/api/employee/delete', this.deleteEmployee);
    }

    public static async employeeList(req: Request, res: Response){
        const employees = await RepositoryService.employee.find();

        return res
                .status(200)
                .json(employees);
    }

    public static async employeeDetail(req: Request, res: Response){
        const employee = await RepositoryService.employee.findById(req.params.id);

        if (employee != null){
            return res
                    .status(200)
                    .json({
                        message: 'Success',
                        employee: employee
                    });
        }else{
            return res
                    .status(200)
                    .json({
                        message: 'failed',
                        reason: 'employee not found'
                    });
        }
    }

    public static async createEmployee(req: Request, res: Response){
        const createResult = await RepositoryService.employee.create(req.body);

        return res
                .status(200)
                .json({
                    message: 'success',
                    insertedData: createResult
                });
    }

    public static async updateEmployee(req: Request, res: Response){
        const updateResult = await RepositoryService.employee.update({_id: req.params.id}, req.body);

        if (updateResult.nModified > 0){
            return res
                    .status(200)
                    .json({
                        message: 'success',
                        updateResult: updateResult
                    });
        }else{
            return res
                    .status(200)
                    .json({
                        message: 'failed',
                        reason: 'employee not found / employee\'s data is up to date'
                    });
        }
    }

    public static async deleteEmployee(req: Request, res: Response){
        let employeeDelCount = 0;
        let attendanceDelCount = 0;

        for (const value of req.body) {
            const employeeDeleteResult = await RepositoryService.employee.delete({_id: value._id});
            const attendanceDeleteResult = await RepositoryService.attendance.delete({employeeId: value._id});

            employeeDelCount += employeeDeleteResult.deletedCount;
            attendanceDelCount += attendanceDeleteResult.deletedCount;
        }

        return res
                .status(200)
                .json({
                    status: 'success',
                    employeeDeletedCount: employeeDelCount,
                    attendanceDeletedCount: attendanceDelCount
                });
    }
}