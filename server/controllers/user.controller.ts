import { Express, Request, Response } from 'express';
import { RepositoryService } from './../services/repository.service';
import * as _ from 'lodash';

export class UserController{
    public static setupRoute(app: Express){
        app.post('/api/login', this.loginProcess);
    }

    public static async loginProcess(req: Request, res: Response){
        const query = {
            username: req.body.username,
            password: req.body.password
        };

        const loginResult = await RepositoryService.user.login(query);

        if (loginResult.length === 1){
            return res
                    .status(200)
                    .json({
                        message: 'success',
                        username: loginResult[0]['username']
                    });
        }else{
            return res
                    .status(200)
                    .json({
                        message: 'failed',
                        reason: 'Username or Password incorrect'
                    });
        }
    }
}