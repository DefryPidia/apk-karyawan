import { Request, Response } from 'express';
import { RepositoryService } from './../services/repository.service';

import * as moment from 'moment';
import * as _ from 'lodash';

export class AttendanceController{
    public static setupRoute(app){
        app.get('/api/attendance', this.attendanceList);
        app.post('/api/attendance', this.createAttendance);
        app.post('/api/attendances', this.createManyAttendance);
        app.put('/api/attendance/:id', this.updateAttendance);
        app.delete('/api/attendance/:id', this.deleteAttendance);
    }

    public static async attendanceList(req: Request, res: Response){
        const baseDate = new Date();
        const year = baseDate.getFullYear();
        const month = baseDate.getMonth();
        const date = baseDate.getDate();
        const employees = await RepositoryService.employee.find().select('name position').lean();

        const currentDate = moment().format('YYYY-MM-DD');
        const tommorowDate = moment(new Date(year, month, date + 1)).format('YYYY-MM-DD');

        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < employees.length; i++){
            employees[i].currentPresentDetail = await RepositoryService.attendance.findOne({
                employeeId: employees[i]._id,
                date: {
                    $lt: moment(tommorowDate).format(),
                    $gt: moment(currentDate).format()
                }
            }).lean();
        }

        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < employees.length; i++){
            if (employees[i].currentPresentDetail != null){
                employees[i].currentPresentDetail.date = moment(employees[i].currentPresentDetail.date).format('MMMM DD, YYYY  (HH:mm:ss a)');
            }
        }

        return res
                .status(200)
                .json(employees);
    }

    public static async createAttendance(req: Request, res: Response){
        const newAttendance = req.body;
        for (const att of newAttendance) {
            att.date = moment().format();
        }

        const createResult = await RepositoryService.attendance.create(newAttendance);

        return res
                .status(200)
                .json({
                    message: 'success',
                    insertedData: createResult
                });
    }

    public static async createManyAttendance(req: Request, res: Response){
      const createResult = await RepositoryService.attendance.createMany(req.body);

      return res
              .status(200)
              .json({
                  message: 'success',
                  createdDataCount: createResult.length
              });
    }

    public static async updateAttendance(req: Request, res: Response){
        const updateResult = await RepositoryService.attendance.update({_id: req.params.id}, req.body);

        if (updateResult.nModified > 0){
            return res
                    .status(200)
                    .json({
                        message: 'success',
                        updateResult: updateResult
                    });
        }else{
            return res
                    .status(200)
                    .json({
                        message: 'failed',
                        reason: 'attendance not found / attendance\'s data is up to date'
                    });
        }
    }

    public static async deleteAttendance(req: Request, res: Response){
        const deleteResult = await RepositoryService.attendance.delete({_id: req.params.id});

        if (deleteResult.deletedCount > 0){
            return res
                    .status(200)
                    .json({
                        message: 'success',
                        deletedAttendanceId: req.params.id
                    });
        }else{
            return res
                    .status(200)
                    .json({
                        message: 'failed',
                        reason: 'Attendance not found / has been deleted'
                    });
        }
    }
}
