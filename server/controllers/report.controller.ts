import { Request, Response, Express } from 'express';
import { RepositoryService } from '../services/repository.service';

import * as moment from 'moment';
import * as _ from 'lodash';
import * as dateHelper from '../helpers/date.helper';

export class ReportController {
    public static setupRoute(app: Express) {
        app.get('/api/report', this.archiveList);
        app.get('/api/report/:year/:month', this.getReport);
    }

    public static async archiveList(req: Request, res: Response) {
        const baseArchiveList = await RepositoryService.attendance
                                .find()
                                .select('-_id date')
                                .lean();

        let archiveList = [];
        /* convert date menjadi hanya year */
        for (const archive of baseArchiveList) {
            const yearArchive = {
                year: moment(archive.date).format('YYYY'),
                months: null
            };

            archiveList.push(yearArchive);
        }
        archiveList = _.uniqBy(archiveList, item => item.year);
        archiveList = archiveList.sort((a, b) => +a.year - +b.year);
        /* ------------------------------- */

        /* mengambil data bulan lalu menyortit dan menconvertnya */
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < archiveList.length; i++) {
            let baseMonthArchives = await RepositoryService.attendance
                                    .find({
                                         date: {
                                            $gte: moment(archiveList[i].year).format(),
                                            $lt: moment(_.toString(+archiveList[i].year + 1)).format()
                                        }
                                                    })
                                    .select('-_id date')
                                    .lean();

            baseMonthArchives = _.uniq(baseMonthArchives.map(item => moment(item.date).format('MM'))).sort((a, b) => +b - +a);
            baseMonthArchives = baseMonthArchives.map(item => moment(item).format('MMMM'));
            archiveList[i].months = baseMonthArchives;
        }
        /* ----------------------------------------------------- */

        return res
                .status(200)
                .json(archiveList);
    }

    public static async getReport(req: Request, res: Response) {
        const year = req.params.year;
        const month = req.params.month;
        const dateQueryStart = moment(new Date(+year, +dateHelper.monthMatcher(month))).format();
        const dateQueryEnd = moment(new Date(+year, +dateHelper.monthMatcher(month) + 1, 0, 23, 59, 59)).format();
        let report = null;

        /* Ambil semua id employee dari collection attendance sesuai tahun dan tanggal */
        report = await RepositoryService.attendance
                                        .find({
                                            date: {
                                                $gte: dateQueryStart,
                                                $lt: dateQueryEnd
                                            }
                                        })
                                        .select('-_id employeeId')
                                        .lean();
        for (const item of report) {
            item.employeeId = item.employeeId.toString();
        }
        report = _.uniqBy(report, 'employeeId');
        /* --------------------------------------------------------------------------- */

        /* Ambil semua nama employee dari collection employee sesuai dengan id di dalam variabel report */
        for (const item of report) {
            item.name = await RepositoryService.employee
                                                .findOne({
                                                    _id: item.employeeId
                                                })
                                                .select('-_id name')
                                                .lean();
        }

        for (const item of report) {
            item.name = item.name.name;
        }
        /* -------------------------------------------------------------------------------------------- */

        /* mengambil attendance di collection attendance sesuai dengan employeeId yang ada di variable report dan menghitungnya */
        // set object
        for (const item of report) {
            item.present = {};
        }

        // cari present "in"
        for (const item of report) {
            item.present.in = await RepositoryService.attendance
                                                        .find({
                                                            employeeId: item.employeeId,
                                                            date: {
                                                                $gte: dateQueryStart,
                                                                $lt: dateQueryEnd
                                                            },
                                                            present: true
                                                        })
                                                        .lean();
        }

        // cari present "permission" dan "inattentive"
        for (const item of report) {
            item.present.permission = [];
            item.present.inattentive = [];

            const takenPresent = await RepositoryService.attendance
                                                        .find({
                                                            employeeId: item.employeeId,
                                                            date: {
                                                                $gte: dateQueryStart,
                                                                $lt: dateQueryEnd
                                                            },
                                                            present: false
                                                        })
                                                        .lean();

            for (const present of takenPresent) {
                if (present.reason === '-') {
                    item.present.inattentive.push(present);
                } else {
                    item.present.permission.push(present);
                }
            }
        }
        /* -------------------------------------------------------------------------------------------------------------------- */

        /* menghitung present "in", "permission", dan "inattentive" */
        for (const item of report) {
            item.present.in = item.present.in.length;
            item.present.permission = item.present.permission.length;
            item.present.inattentive = item.present.inattentive.length;
        }
        /* -------------------------------------------------------- */

        /* memangkas employeeId yang ada di variable report menjadi 6 kata dari belakang */
        for (const item of report) {
            item.employeeId = item.employeeId.slice(-6);
        }
        /* ----------------------------------------------------------------------------- */

        return res
                .status(200)
                .json(report);
    }
}