export class BaseRepository{
    constructor(public entity: any){}

    find(query: any = {}){
        return this.entity.model.find(query);
    }

    findOne(query: any = {}){
        return this.entity.model.findOne(query);
    }

    findById(id: any){
        return this.entity.model.findById(id);
    }

    create(document: any = {}){
        return this.entity.model.create(document);
    }

    createMany(documents: any = []){
        return this.entity.model.insertMany(documents);
    }

    update(target: any = {}, newData: any = {}){
        return this.entity.model.updateOne(target, {$set: newData});
    }

    delete(target: any = {}){
        return this.entity.model.deleteMany(target);
    }
}
