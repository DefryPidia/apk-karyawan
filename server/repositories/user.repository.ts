import { BaseRepository } from './base.repository';
import { UserEntity } from './../entities/user.entity';

export class UserRepository extends BaseRepository{
    constructor(){
        super(UserEntity);
    }

    login(query: any = {}){
        return UserEntity.model.find({
            username: query.username,
            password: query.password
        });
    }
}