import { BaseRepository } from './base.repository';
import { EmployeeEntity } from './../entities/employee.entity';

export class EmployeeRepository extends BaseRepository{
    constructor(){
        super(EmployeeEntity);
    }
}