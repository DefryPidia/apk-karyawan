import { AttendanceEntity } from './../entities/attendance.entity';
import { BaseRepository } from './base.repository';

export class AttendanceRepository extends BaseRepository{
    constructor(){
        super(AttendanceEntity);
    }
}