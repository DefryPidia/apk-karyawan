import { DatabaseService } from './services/database.service';
import { EmployeeController } from './controllers/employee.controller';
import { UserController } from './controllers/user.controller';
import { AttendanceController } from './controllers/attendance.controller';
import { ReportController } from './controllers/report.controller';

import * as express from 'express';
import * as bodyParser from 'body-parser';

const boot = async () => {
    const app = express();
    app.use(bodyParser.json());

    await DatabaseService.setup();

    UserController.setupRoute(app);
    EmployeeController.setupRoute(app);
    AttendanceController.setupRoute(app);
    ReportController.setupRoute(app);

    app.listen(3000, () => console.log('Server is running on port 3000'));
};

boot();