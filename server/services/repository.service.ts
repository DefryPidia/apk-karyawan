import { EmployeeRepository } from './../repositories/employee.repository';
import { UserRepository } from './../repositories/user.repository';
import { AttendanceRepository } from './../repositories/attendance.repository';

export class RepositoryService{
    public static employee = new EmployeeRepository();
    public static user = new UserRepository();
    public static attendance = new AttendanceRepository();
}