import * as mongoose from 'mongoose';

export class DatabaseService{
    public static mongoose: mongoose.Mongoose;

    public static async setup(){
        this.mongoose = await mongoose.connect('mongodb://localhost/apk-karyawan', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
    }
}