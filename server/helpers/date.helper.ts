export function monthMatcher(month) {
    switch (month.toLowerCase()) {
        case 'january':
        case 'januari':
            return 0;
        case 'february' :
        case 'februari':
            return 1;
        case 'march':
        case 'maret':
            return 2;
        case 'april':
            return 3;
        case 'may':
        case 'mei':
            return 4;
        case 'june':
        case 'juni':
            return 5;
        case 'july':
        case 'juli':
            return 6;
        case 'august':
        case 'agustus':
            return 7;
        case 'september':
            return 8;
        case 'october':
        case 'oktober':
            return 9;
        case 'november':
            return 10;
        case 'december':
        case 'desember':
            return 11;
        default:
            return 'Month not found';
    }
}

export default monthMatcher;