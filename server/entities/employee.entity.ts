import { Schema } from 'mongoose';
import { DatabaseService } from './../services/database.service';

export class EmployeeEntity{
    public static schema = new Schema({
        name: String,
        position: String,
        email: String,
        address: String,
    }, {
        versionKey: false,
        collection: 'employee'
    });

    public static get model(){
        return DatabaseService.mongoose.model('employee', this.schema);
    }
}



