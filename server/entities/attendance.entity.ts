import { Schema } from 'mongoose';
import { DatabaseService } from './../services/database.service';

export class AttendanceEntity{
    public static schema = new Schema({
        employeeId: {
            type: Schema.Types.ObjectId,
            required: true
        },
        date: {
            type: Date,
            required: true
        },
        present: {
            type: Boolean,
            required: true
        },
        reason: {
            type: String,
            default: '-'
        }
    }, {
        versionKey: false,
        collection: 'attendance'
    });

    public static get model(){
        return DatabaseService.mongoose.model('attendance', this.schema);
    }
}