import { Schema } from 'mongoose';
import { DatabaseService } from './../services/database.service';

export class UserEntity{
    public static schema = new Schema({
        username: String,
        password: String
    }, {
        collection: 'user'
    });

    public static get model(){
        return DatabaseService.mongoose.model('user', this.schema);
    }
}

