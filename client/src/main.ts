// angularcomponents
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// modules
import { AuthModule } from './main/auth/auth.module';

// environments
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AuthModule)
  .catch(err => console.error(err));
