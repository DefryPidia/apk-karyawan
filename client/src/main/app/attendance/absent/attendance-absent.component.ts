// angularcomponents
import { Component, ElementRef, Input, ViewChild, OnInit } from '@angular/core';

// services
import { AttendanceRestService } from '../attendance-rest.service';
import { AttendanceListService } from '../list/attendance-list.service';

// sweetalert
import Swal from 'sweetalert2';

@Component({
  selector: 'app-attendance-absent',
  templateUrl: './attendance-absent.component.html',
  providers: []
})

export class AttendanceAbsentComponent implements OnInit{

  @ViewChild('confirm') confirm: any;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('textArea') textArea: ElementRef;
  @Input() formValue: Partial<any>;

  public id: any;
  public reason = '';
  public employeeAttendances: any = [];
  public desc: any;
  public submitStatus = true;
  public currentEmployeeLength: number;
  public currentEmployeeDefault = 0;
  public displayAbsent: boolean;
  public timerChecker: any;
  public radioIn: boolean;
  public switchCondition = false;

  constructor(
    private atdRstS: AttendanceRestService,
    private atdLstS: AttendanceListService,
  ) { }

  ngOnInit(): void { }

  showModal(data: any): void {
    this.displayAbsent = true;
    this.employeeAttendances = data;
    this.currentEmployeeLength = data.length;
    this.timerChecker = setInterval(() => {
      this.submitStatus = this.fullAttendanceChecker();
    });
  }

  closeModal(): void {
    this.displayAbsent = false;
    this.submitStatus = true;
    this.employeeAttendances = null;
    this.currentEmployeeDefault = 0;
    clearInterval(this.timerChecker);
    this.radioIn = false;
    this.switchCondition = false;
    console.log(this.switchCondition);
  }

  enable(status, index): void{
    this.employeeAttendances[index].currentPresentDetail.present = status;
    this.currentEmployeeDefault++;
    if (this.currentEmployeeDefault >= this.currentEmployeeLength){
      this.submitStatus = false;
    }
  }

  hidden(i): any {
    if (this.employeeAttendances[i].currentPresentDetail.present ===
       'permission') {
      return 'block';
    } else {
      return 'none';
    }
  }

  fullAttendanceChecker(): any {
    let finalStatus = true;

    for ( const value of this.employeeAttendances ) {
      if ( value.currentPresentDetail.present === 'inattentive' || value.currentPresentDetail.present === true) {
        finalStatus = false;
      } else if ( value.currentPresentDetail.present === 'permission' && value.currentPresentDetail.reason !== '' ) {
        finalStatus = false;
      } else if ( value.currentPresentDetail.present === '' ) {
        finalStatus = true;
        break;
      } else {
        finalStatus = true;
        break;
      }
    }
    return finalStatus;
  }

  allIn(switchCondition: boolean): void {
    if (switchCondition === false) {
      for ( const emps of this.employeeAttendances) {
        emps.currentPresentDetail.present = true;
      }
      this.radioIn = true;
      this.switchCondition = true;
    } else {
      for ( const emps of this.employeeAttendances) {
        emps.currentPresentDetail.present = '';
      }
      this.radioIn = false;
      this.switchCondition = false;
    }
  }

  insertAttendance(employeeAttendances): void{
    const newAttendances = [];

    for ( const emp of employeeAttendances){
      if ( emp.currentPresentDetail.present === 'permission' && emp.currentPresentDetail.reason === '' ) {
        this.submitStatus = true;
        break;
      } else {
        const newObj = {
          employeeId: emp._id,
          reason: (emp.currentPresentDetail.present === 'permission') ? emp.currentPresentDetail.reason : '-',
          present: (emp.currentPresentDetail.present === true) ? true : false
        };
        newAttendances.push(newObj);
      }
    }

    this.atdRstS.create(newAttendances).subscribe(res => {
      this.closeModal();
      this.atdLstS.notifyPostAddition();
      Swal.fire({
        icon: 'success',
        title: '- SUCCESS TO ABSENT -',
        showConfirmButton: false,
        timer: 1500
      });
    });
  }
}
