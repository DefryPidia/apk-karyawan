// angularcomponents
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-attendance-detail',
  templateUrl: './attendance-detail.component.html',
  providers: []
})

export class AttendanceDetailComponent implements OnInit {

  @Input() formValue: Partial<any>;

  public detail: any;
  public displayDetail: boolean;

  constructor( ) { }

  ngOnInit(): void { }

  showModal(att): void {
    this.detail = att;
    this.displayDetail = true;
  }

  closeModal(): void {
    this.displayDetail = false;
  }

  presenceChecker(status): any {
    if (status.present === true){
      return 'In';
    } else if (status.present === false && status.reason === '-') {
      return 'Inattentive';
    } else {
      return `Permission (${status.reason})`;
    }
  }
}
