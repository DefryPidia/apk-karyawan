// angularcomponents
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AttendanceRestService {

  constructor(
      private http: HttpClient
  ) { }

  getAttendance(): any {
    return this.http.get('/api/attendance', {});
  }
  create(data): any {
      return this.http.post('/api/attendance', data);
  }
  update(id, data): any {
      return this.http.put('/api/attendance/' + id, data);
  }
  delete(id): any {
    return this.http.delete('/api/attendance/' + id);
  }
}
