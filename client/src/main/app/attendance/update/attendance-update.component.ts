// angularcomponents
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

// components
import { AttendanceListComponent } from '../list/attendance-list.component';

// services
import { AttendanceRestService } from '../attendance-rest.service';
import { AttendanceListService } from '../list/attendance-list.service';
import Swal from 'sweetalert2';

// models
import { Attendance } from '../attendance.model';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-attendance-update',
  templateUrl: './attendance-update.component.html'
})

export class AttendanceUpdateComponent implements OnInit{

  @ViewChild('confirm') confirm: any;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('textArea') textArea: ElementRef;

  public attendanceTobeUpdated: any;
  public displayUpdate: boolean;
  public timerChecker: any;
  public id: any;
  public submitStatus: boolean;
  public reason = '';

  constructor(
    private atdRstS: AttendanceRestService,
    private atdLstS: AttendanceListService,
    private atdLstC: AttendanceListComponent
    ) {
      this.id = '';
    }

  ngOnInit(): void {
    this.atdLstC.getAttendance();
    this.atdLstS.postAddedObservable.subscribe(res => {
      this.atdLstC.getAttendance();
    });
    this.atdLstS.newIdNotify.subscribe(res => {
      this.id = this.atdLstS.id;
    });
  }

  showModal(attendance: Attendance): void {
    this.attendanceTobeUpdated = {...attendance};
    this.displayUpdate = true;
    this.timerChecker = setInterval(() => {
      this.submitStatus = this.fullAttendanceChecker(this.attendanceTobeUpdated);
    });
  }

  closeModal(): void {
    this.displayUpdate = false;
    this.attendanceTobeUpdated = null;
    this.submitStatus = true;
    clearInterval(this.timerChecker);
  }

  enable(identity, status): void{
    if (identity === 'enable'){
      this.textArea.nativeElement.disabled = false;
    } else {
      this.textArea.nativeElement.disabled = true;
    }
    this.addAttendanceStatus(status);
    this.attendanceTobeUpdated.currentPresentDetail.reason = '';
    this.submitStatus = false;
  }

  addAttendanceStatus(status): void{
    this.attendanceTobeUpdated.currentPresentDetail.present = status;
  }

  fullAttendanceChecker(attendance: any): any {
    let finalStatus = true;
    const presentDetail = [attendance.currentPresentDetail];

    for ( const value of presentDetail ) {
      if ( value.present === 'inattentive' || value.present === true) {
        finalStatus = false;
      } else if ( value.present === 'permission' && value.reason !== '' ) {
        finalStatus = false;
      } else if ( value.present === '' ) {
        finalStatus = true;
        break;
      } else {
        finalStatus = true;
        break;
      }
    }

    return finalStatus;
  }

  submit(data: Attendance): void {
    const newAttendance = {
      present: (data.currentPresentDetail.present === true) ? true : false,
      reason: (data.currentPresentDetail.reason === '') ? '-' : data.currentPresentDetail.reason
    };

    this.atdRstS.update(data.currentPresentDetail._id, newAttendance).subscribe(res => {
      this.closeModal();
      Swal.fire({
        icon: 'success',
        title: '- SUCCESS UPDATE ABSENT -',
        showConfirmButton: false,
        timer: 1500
      });
      this.atdLstS.notifyPostAddition();
    });
  }
}
