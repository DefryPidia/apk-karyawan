// angularcomponents
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

// components
import { DeleteComponent } from 'src/libraries/delete/delete-confirm.component';
import { AttendanceUpdateComponent } from '../update/attendance-update.component';
import { AttendanceDetailComponent } from '../detail/attendance-detail.component';
import { AttendanceAbsentComponent } from '../absent/attendance-absent.component';

// services
import { AttendanceListService } from './attendance-list.service';
import { AttendanceRestService } from '../attendance-rest.service';

// models
import { Attendance } from '.././attendance.model';

@Component({
  selector: 'app-attendance-list',
  templateUrl: './attendance-list.component.html',
  providers: [
    AttendanceListService
  ]
})

export class AttendanceListComponent implements OnInit {

  @ViewChild(AttendanceAbsentComponent) atdAbsC: AttendanceAbsentComponent;
  @ViewChild(AttendanceUpdateComponent) atdUdtC: AttendanceUpdateComponent;
  @ViewChild(AttendanceDetailComponent) atdDtlC: AttendanceDetailComponent;
  @ViewChild(DeleteComponent) dltCfrm: DeleteComponent;
  @ViewChild('addPost') addBtn: ElementRef;

  public attendance: any = [];
  public first = 0;
  public rows = 10;
  public data: any = [];

  constructor(
    private atdLstS: AttendanceListService,
    private atdRstS: AttendanceRestService
  ) {
      this.atdLstS.postEditObservable.subscribe(res => {
        this.addBtn.nativeElement.click();
      });
    }

  ngOnInit(): void {
    this.getAttendance();
    this.atdLstS.postAddedObservable.subscribe(res => {
      this.getAttendance();
      this.data = [];
    });
    this.atdLstS.postDeleteObservable.subscribe(res => {
      this.getAttendance();
    });
  }

  getAttendance(): void {
    this.atdRstS.getAttendance().subscribe(res => {
      this.attendance = res;
    });
  }

  // showDeleteConfirm(id): void {
  //   this.dltCfrm.showConfirm(id, 'attendance');
  // }

  showAbsentModal(): void {
    const selectedEmployees = [];

    for (const da of this.data){
      if (da.currentPresentDetail == null){
        selectedEmployees.push(Object.assign({}, da));
      }
    }
    for (const emps of selectedEmployees){
      emps.currentPresentDetail = {
        present: '',
        reason: ''
      };
    }
    this.atdAbsC.showModal(selectedEmployees);
  }

  showUpdateModal(data: Attendance): void {
    this.atdUdtC.showModal({...data});
  }

  showDetailModal(att: any): void {
    this.atdDtlC.showModal({...att});
  }

  attendanceChecker(status): any {
    if (status == null){
      return 'not yet';
    }else{
      return 'already';
    }
  }

  statusChecker(): any {
    let result: boolean;

    for (const atd of this.attendance) {
      if (atd.currentPresentDetail === null) {
        result = false;
        break;
      } else {
        result = true;
      }
    }
    return result;
  }

  test(data): void {
    if (data.checked === true){
      for (let i = 0; i < this.data.length; i++){
        if (this.data[i].currentPresentDetail?.reason){
          this.data.splice(i, 1);
        }
      }
    }
  }
}
