// angularcomponents
import { Injectable } from '@angular/core';

// rxjs
import { Subject } from 'rxjs';

// models
import { Attendance } from '../attendance.model';

@Injectable()
export class AttendanceListService {

  public postAddedObservable = new Subject();
  public postEditObservable = new Subject();
  public postDeleteObservable = new Subject();
  public postToBeEdited = new Attendance();
  public newIdNotify = new Subject();
  public id: any = '';

  constructor( ) {
      this.postToBeEdited = new Attendance();
    }

  notifyPostDeletion(): void {
      this.postDeleteObservable.next();
  }

  notifyPostAddition(): void {
      this.postAddedObservable.next();
  }

  notifyPostEdit(): void {
      this.postEditObservable.next();
  }

  setPostToEdit(post: Attendance): void {
      this.postToBeEdited = post;
      this.notifyPostEdit();
  }

  setDelete(id: any): void {
    this.id = id;
    this.newIdNotify.next();
  }
}
