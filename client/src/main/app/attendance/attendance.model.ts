export class Attendance {
    constructor() {
        this._id = '';
        this.name = '';
        this.position = '';
        this.currentPresentDetail = {
            _id: '',
            present: '',
            reason: ''
        };
    }
    // tslint:disable-next-line: variable-name
    public _id;
    public name;
    public position;
    public currentPresentDetail;
}
