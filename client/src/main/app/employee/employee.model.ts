export class Employee {
  constructor() {
      this._id = '';
      this.name = '';
      this.position = '';
      this.email = '';
      this.address = '';
  }
  // tslint:disable-next-line: variable-name
  public _id;
  public name;
  public position;
  public email;
  public address;
}
