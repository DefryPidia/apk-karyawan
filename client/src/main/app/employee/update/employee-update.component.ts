// angularcomponents
import { Component, ViewChild } from '@angular/core';

// components
import { EmployeeFormComponent } from '../form/employee-form.component';

// services
import { EmployeeRestService } from '../employee-rest.service';
import { EmployeeListService } from '../list/employee-list.service';
import Swal from 'sweetalert2';

// models
import { Employee } from '../employee.model';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html'
})
export class EmployeeUpdateComponent {

  @ViewChild(EmployeeFormComponent) empFrmC: EmployeeFormComponent;
  public employeeTobeUpdated: any;
  public displayUpdate: boolean;

  constructor(
    private empRstS: EmployeeRestService,
    private empLstS: EmployeeListService,
    ) { }

  showModal(employee: Employee): void {
    this.employeeTobeUpdated = {...employee};
    this.displayUpdate = true;
  }

  closeModal(): void {
    this.displayUpdate = false;
    this.clearFormValue();
  }

  clearFormValue(): any {
    this.empFrmC.formValue = {
      desc: 'update'
    };
  }

  submit(formValue: any): void {
    this.empRstS.update(this.employeeTobeUpdated._id, formValue).subscribe(res => {
      this.closeModal();
      Swal.fire({
        icon: 'success',
        title: '- SUCCESS UPDATE DATA -',
        showConfirmButton: false,
        timer: 1500
      });
      this.empLstS.notifyPostAddition();
    });
  }
}
