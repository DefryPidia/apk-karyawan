// angularcomponents
import { Injectable } from '@angular/core';

// rxjs
import { Subject } from 'rxjs';

@Injectable()
export class EmployeeFormService {

  public newIdNotify = new Subject();
  public id: any = '';

  constructor() { }

  setDelete(id: any): void {
      this.id = id;
      this.newIdNotify.next();
  }
}
