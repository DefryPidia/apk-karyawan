// angularcomponents
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';

// components
import { EmployeeListComponent } from '../list/employee-list.component';

// services
import { EmployeeListService } from '../list/employee-list.service';
import { EmployeeFormService } from './employee-form.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  providers: []
})

export class EmployeeFormComponent implements OnInit {

  @ViewChild('confirm') confirm: any;
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @Output() submitClick = new EventEmitter();
  @Input() formType: 'create' | 'update' = 'create';
  @Input() formValue: Partial<any> = {};

  public id: any;

  constructor(
    private empLstS: EmployeeListService,
    private empLstC: EmployeeListComponent,
    private empFrmS: EmployeeFormService,
  ) {
      this.id = '';
    }

  ngOnInit(): void {  }
}
