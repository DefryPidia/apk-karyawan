// angularcomponents
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeRestService {

  constructor(
      private http: HttpClient
  ) { }

  getEmployee(): any {
      return this.http.get('/api/employee', {});
  }

  create(data): any {
      return this.http.post('/api/employee', data);
  }

  update(id, data): any {
      return this.http.put('/api/employee/' + id, data);
  }

  delete(datas): any{
    return this.http.post('/api/employee/delete', datas);
  }
}
