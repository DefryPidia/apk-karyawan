// angular components
import { Component, ViewChild } from '@angular/core';

// components
import { EmployeeFormComponent } from '../form/employee-form.component';

// services
import { EmployeeRestService } from '../employee-rest.service';
import { EmployeeListService } from '../list/employee-list.service';

// models
import { Employee } from '../employee.model';

// sweetalert
import Swal from 'sweetalert2';


@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  providers: []
})

export class EmployeeCreateComponent {
  
  @ViewChild(EmployeeFormComponent) empFrmC: EmployeeFormComponent;
  public displayCreate: boolean;

  constructor(
    private empRstS: EmployeeRestService,
    private empLstS: EmployeeListService
  ) { }

  showModal(): void {
    this.displayCreate = true;
  }

  closeModal(): void {
    this.displayCreate = false;
    this.clearFormValue();
  }

  clearFormValue(): any {
    this.empFrmC.formValue = {
      desc: 'add'
    };
  }

  submit(formValue: Employee): void {
    if (formValue.name && formValue.position && formValue.email && formValue.address){
      this.empRstS.create(formValue).subscribe(res => {
        this.closeModal();
        Swal.fire({
          icon: 'success',
          title: '- SUCCESS ADD DATA -',
          showConfirmButton: false,
          timer: 1500
        });
        this.empLstS.notifyPostAddition();
      });
    }
  }
}
