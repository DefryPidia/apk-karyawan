// angularcomponents
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

// models
import { Employee } from '../employee.model';

@Injectable()
export class EmployeeListService {

  public postAddedObservable = new Subject();
  public postEditObservable = new Subject();
  public postDeleteObservable = new Subject();
  public postToBeEdited = new Employee();

  constructor( ) {
      this.postToBeEdited = new Employee();
    }

  notifyPostDeletion(): void {
      this.postDeleteObservable.next();
  }

  notifyPostAddition(): void {
      this.postAddedObservable.next();
  }

  notifyPostEdit(): void {
      this.postEditObservable.next();
  }

  setPostToEdit(post: Employee): void {
      this.postToBeEdited = post;
      this.notifyPostEdit();
  }
}
