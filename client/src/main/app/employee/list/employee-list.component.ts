// angularcomponents
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

// components
import { DeleteComponent } from 'src/libraries/delete/delete-confirm.component';
import { EmployeeCreateComponent } from '../create/employee-create.component';
import { EmployeeUpdateComponent } from '../update/employee-update.component';

// services
import { EmployeeListService } from './employee-list.service';
import { EmployeeRestService } from '../employee-rest.service';
import { EmployeeFormService } from '../form/employee-form.service';

// models
import { Employee } from '.././employee.model';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  providers: [
    EmployeeListService,
    EmployeeFormService
  ]
})

export class EmployeeListComponent implements OnInit {

  @ViewChild(EmployeeCreateComponent) empCrtC: EmployeeCreateComponent;
  @ViewChild(EmployeeUpdateComponent) empUdtC: EmployeeUpdateComponent;
  @ViewChild(DeleteComponent) dltCfrm: DeleteComponent;
  @ViewChild('addPost') addBtn: ElementRef;

  constructor(
    private empLstS: EmployeeListService,
    private empRstS: EmployeeRestService
  ) {
      this.empLstS.postEditObservable.subscribe(res => {
        this.addBtn.nativeElement.click();
      });
    }

  public employees: any = [];
  public first = 0;
  public rows = 10;
  public dataEmp: any[];

  // tslint:disable-next-line: typedef
  ngOnInit() {
    this.getEmployee();
    this.empLstS.postAddedObservable.subscribe(res => {
      this.getEmployee();
    });
    this.empLstS.postDeleteObservable.subscribe(res => {
      this.getEmployee();
      this.dataEmp = [];
    });
  }

  getEmployee(): void {
    this.empRstS.getEmployee().subscribe(res => {
      this.employees = res;
    });
  }

  editEmployee(employee: Employee): void {
    this.empLstS.setPostToEdit(employee);
  }

  showDeleteConfirm(id): void {
    this.dltCfrm.showConfirm(id, 'employee');
  }

  showCreateModal(): void {
    this.empCrtC.showModal();
  }

  showUpdateModal(data: Employee): void {
    this.empUdtC.showModal(data);
  }

  deleteAllEmploye(dataEmp): void {
    this.dltCfrm.showConfirm(dataEmp, 'employee');
  }

  hiddenCheck(): any {
    let result: boolean;

    if (this.employees.length === 0) {
      result = true;
    } else {
      result = false;
    }
    return result;
  }
}
