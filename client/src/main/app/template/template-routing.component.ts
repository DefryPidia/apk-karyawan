import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { EmployeeListComponent } from './../employee/list/employee-list.component';
import { AttendanceListComponent } from './../attendance/list/attendance-list.component';
import { AboutComponent } from './../about/about.component';
import { ReportListComponent } from '../report/list/report-list.component';

const routes: Routes = [
    {
        path: 'employee',
        component: EmployeeListComponent
    },
    {
        path: 'attendance',
        component: AttendanceListComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'report',
        component: ReportListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TemplateRoutingModule { }
