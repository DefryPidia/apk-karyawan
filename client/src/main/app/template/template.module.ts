// angularcomponents
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

// components
import { TemplateComponent } from './template.component';
import { EmployeeListComponent } from './../employee/list/employee-list.component';
import { EmployeeUpdateComponent } from './../employee/update/employee-update.component';
import { EmployeeFormComponent } from './../employee/form/employee-form.component';
import { EmployeeCreateComponent } from './../employee/create/employee-create.component';
import { AttendanceListComponent } from './../attendance/list/attendance-list.component';
import { AttendanceUpdateComponent } from './../attendance/update/attendance-update.component';
import { AttendanceDetailComponent } from './../attendance/detail/attendance-detail.component';
import { AttendanceAbsentComponent } from './../attendance/absent/attendance-absent.component';
import { AboutComponent } from './../about/about.component';
import { ContentComponent } from './../about/content/content.component';
import { ReportListComponent } from '../report/list/report-list.component';
import { ReportMonthComponent } from '../report/month/report-month.component';
import { ReportPreviewComponent } from '../report/preview/report-preview.component';

// Routing
import { TemplateRoutingModule } from './template-routing.component';

// componentmodules
import { LibrariesModule } from './../../../libraries/libraries.module';

// primengmodules
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from 'primeng/sidebar';
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { PaginatorModule } from 'primeng/paginator';
import { DataViewModule } from 'primeng/dataview';
import { ListboxModule } from 'primeng/listbox';
import { InputSwitchModule } from 'primeng/inputswitch';

// service
import { EmployeeRestService } from '../employee/employee-rest.service';
import { AttendanceRestService } from '../attendance/attendance-rest.service';
import { EmployeeListService } from '../employee/list/employee-list.service';
import { AttendanceListService } from '../attendance/list/attendance-list.service';
import { ReportRestService } from '../report/report-rest.service';

@NgModule({
  declarations: [
    TemplateComponent,
    EmployeeListComponent,
    EmployeeUpdateComponent,
    EmployeeFormComponent,
    EmployeeCreateComponent,
    AttendanceListComponent,
    AttendanceUpdateComponent,
    AttendanceDetailComponent,
    AttendanceAbsentComponent,
    AboutComponent,
    ContentComponent,
    ReportListComponent,
    ReportMonthComponent,
    ReportPreviewComponent
  ],
  imports: [
    TemplateRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CardModule,
    ButtonModule,
    CommonModule,
    NgbModule,
    SidebarModule,
    MenubarModule,
    TableModule,
    ToolbarModule,
    InputTextModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    DropdownModule,
    DialogModule,
    CalendarModule,
    LibrariesModule,
    BrowserAnimationsModule,
    PaginatorModule,
    DataViewModule,
    ListboxModule,
    InputSwitchModule,
    RouterModule
  ],
  exports: [
    TemplateComponent
  ],
  providers: [
    EmployeeRestService,
    AttendanceRestService,
    EmployeeListService,
    AttendanceListService,
    ReportRestService
  ],
  bootstrap: [TemplateComponent]
})

export class TemplateModule { }
