// angularcomponents
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

// components
import { LogoutComponent } from '../../../libraries/logout/logout-confirm.component';

// other plugin
import { MenuItem } from 'primeng/api';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html'
})
export class TemplateComponent {

  public title = 'client';
  public items: MenuItem[];
  public display;
  public date = new Date();
  public displayCalendar: boolean;

  @ViewChild(LogoutComponent) lgtCfrm: LogoutComponent;
  @ViewChild('btnClose') btnClose: ElementRef;

  constructor(
    private router: Router
  ) {
      if (!localStorage.getItem('loggedInUser')){
          this.router.navigate(['/']);
          Swal.fire({
            title: '- FAILED ACCESS PAGE -',
            text: 'Please to SIGN IN first for access this page :)',
            icon: 'warning',
            showConfirmButton: true
          });
      }
    }

  getSide(): void {
      this.display = 'true';
  }

  calendarDisplay(): void {
    this.displayCalendar = true;
  }

  showLogoutConfirm(): void {
    this.lgtCfrm.showConfirm();
  }

  closeNav(): void{
    this.btnClose.nativeElement.click();
  }
}
