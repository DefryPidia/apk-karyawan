// angularcomponents
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

// components
import { LogoutComponent } from 'src/libraries/logout/logout-confirm.component';

// primengcomponents
import { MenuItem } from 'primeng/api';
import Swal from 'sweetalert2';
import * as _ from 'lodash-es';

@Component({
    selector: 'app-about-template',
    templateUrl: './about.component.html'
})

export class AboutComponent implements OnInit{

  @ViewChild(LogoutComponent) lgtCfrm: LogoutComponent;

  public displayCalendar: boolean;
  public display;
  public items: MenuItem[];
  public date = new Date();
  public baseEmployee = [
      {
          name: 'Defry Galuh Pidia',
          position: 'Fullstack Developer',
          description: 'Hi my name is Defry',
          imgUrl: '../../../assets/img/defra.jpg'
      },
      {
          name: 'Dicky Lukman Hakim',
          position: 'Frontend & UI/UX',
          description: 'Hi my name is Dicky',
          imgUrl: '../../../assets/img/dickyl.jpg'
      },
      {
          name: 'Muhammad Ihsan',
          position: 'Frontend & UI/UX',
          description: 'Hi my name is Ihsan',
          imgUrl: '../../../assets/img/mIhsan.jpg'
      }
  ];
  public displayEmployee = _.shuffle(this.baseEmployee);

  constructor(
    private router: Router
  ) {
      if (!localStorage.getItem('loggedInUser')){
          this.router.navigate(['/']);
          Swal.fire({
            title: '- FAILED ACCESS PAGE -',
            text: 'Please to SIGN IN first for access this page :)',
            icon: 'warning',
            showConfirmButton: true
          });
      }
    }

  ngOnInit(): void{
  }

  getSide(): void {
      this.display = 'true';
  }

  showLogoutConfirm(): void {
    this.lgtCfrm.showConfirm();
  }

  calendarDisplay(): void {
      this.displayCalendar = true;
  }
}
