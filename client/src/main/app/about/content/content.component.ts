import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../../employee/employee.model';


@Component({
    selector: 'app-content',
    templateUrl: './content.component.html'
})
export class ContentComponent implements OnInit{
    @Input() employee: Array<any>;

    ngOnInit(): void {  }
}
