// angular components
import { Component, Input, OnInit } from '@angular/core';

// moment
import * as moment from 'moment';

@Component({
    selector: 'app-report-month',
    templateUrl: './report-month.component.html'
})

export class ReportMonthComponent implements OnInit {

    @Input() year: number;
    @Input() months: any[];

    public displayMonth: boolean;
    public selectedMonth: any = null;

    constructor( ) { }

    ngOnInit(): void { }

    showModalReport(): void {
        this.displayMonth = true;
    }

    closeModal(): void {
        this.displayMonth = false;
        this.selectedMonth = null;
    }

    sendMonth(url): void {
        url = url + this.year + '/' + this.selectedMonth;
        window.open(url, '_blank');
        this.closeModal();
    }

    onRowSelect(event): void {
        this.selectedMonth = event.data.name;
    }

    onRowUnselect(event): void {
        this.selectedMonth = null;
    }

    latestMonth(month): any {
        const currentMonth = moment().format('MMMM').toLowerCase();
        const months = month.toLowerCase();

        if (currentMonth === months) {
            return month + ' (Latest)';
        } else {
            return month;
        }
    }
}
