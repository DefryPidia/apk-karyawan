import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReportRestService {
    constructor(
        private http: HttpClient
    ) { }

    getReport(): any {
        return this.http.get('api/report', {});
    }

    getSpecificReport(year, month): any {
        return this.http.get(`api/report/${year}/${month}`, {});
    }
}
