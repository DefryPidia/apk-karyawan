// angular components
import { Component, OnInit, ViewChild } from '@angular/core';

// components
import { ReportMonthComponent } from '../month/report-month.component';

// services
import { ReportRestService } from '../report-rest.service';


@Component({
    selector: 'app-report-list',
    templateUrl: './report-list.component.html'
})

export class ReportListComponent implements OnInit{

    @ViewChild(ReportMonthComponent) rptMthC: ReportMonthComponent;

    public reportList: any;
    public selectedYear: number;
    public selectedMonths: any[];

    constructor(
        private reportRestService: ReportRestService
     ) { }

    ngOnInit(): void {
        this.getReportList();
    }

    getReportList(): void {
        this.reportRestService.getReport().subscribe(res => {
            this.reportList = res;
        });
    }

    showModalReport(year: number, months: string[]): void {
        const keyedMonths = [];
        for ( const [index, value] of months.entries() ) {
            const monthObj = {
                no: index,
                name: value
            };
            keyedMonths.push(monthObj);
        }

        this.rptMthC.showModalReport();
        this.selectedYear = year;
        this.selectedMonths = keyedMonths;
    }
}
