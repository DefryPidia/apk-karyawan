// angular components
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// services
import { ReportRestService } from '../report-rest.service';

@Component({
    selector: 'app-report-preview',
    templateUrl: './report-preview.component.html'
})
export class ReportPreviewComponent implements OnInit {

    public year: any;
    public month: any;
    public reports: any[] = [];

    constructor(
        private route: ActivatedRoute,
        private rptRstS: ReportRestService
    ) {}

    ngOnInit(): void {
        this.route.paramMap.subscribe(route => {
            this.year = route.get('year');
            this.month = route.get('selectedMonth');
        });
        this.getReport();
        window.print();
        window.onafterprint = () => {
            window.close();
        };
    }

    getReport(): void {
        this.rptRstS.getSpecificReport(this.year, this.month).subscribe(res => {
            this.reports = res;
        });
    }
}
