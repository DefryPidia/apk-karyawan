import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { TemplateModule } from '../app/template/template.module';

import { AuthRoutingModule } from './auth-routing.module';

import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';

// PrimeNg Module
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from 'primeng/sidebar';
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
    declarations: [
        LoginComponent,
        AuthComponent
    ],
    imports: [
        BrowserModule,
        RouterModule,
        AuthRoutingModule,
        FormsModule,
        HttpClientModule,
        CardModule,
        ButtonModule,
        NgbModule,
        SidebarModule,
        MenubarModule,
        TableModule,
        ToolbarModule,
        InputTextModule,
        MessagesModule,
        MessageModule,
        ToastModule,
        DropdownModule,
        DialogModule,
        CalendarModule,
        TemplateModule
    ],
    providers: [ ],
    bootstrap: [AuthComponent]
})
export class AuthModule { }
