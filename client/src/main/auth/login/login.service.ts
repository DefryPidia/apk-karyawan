// angularcomponents
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// models
import { User } from './login.model';

@Injectable()
export class LoginService {

  constructor(
      private http: HttpClient
  ) { }

  validateLogin(user: User): any{
      return this.http.post('api/login', {
          username : user.username,
          password : user.password
      });
  }
}
