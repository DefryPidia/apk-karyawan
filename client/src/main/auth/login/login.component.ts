// angularcomponents
import { Component, OnInit } from '@angular/core';
import { User } from './login.model';
import { Router } from '@angular/router';

// services
import { LoginService } from './login.service';

// sweetalert
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public user: User;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) {
      this.user = new User();
    }

  validateLogin(): void {
    if (this.user.username && this.user.password) {
      this.loginService.validateLogin(this.user).subscribe(result => {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
          }
        });

        Toast.fire({
          icon: 'success',
          title: `- WELCOME ${this.user.username} -`
        });

         // tslint:disable-next-line: no-string-literal
        if (result['message'] === 'success') {
          localStorage.setItem('loggedInUser', this.user.username);
          this.router.navigate(['/employee']);
        } else {
        Swal.fire({
          title: '- FAILED TO SIGN IN -',
          text: 'Please enter a correct Username and Password :)',
          icon: 'error',
          showConfirmButton: true
        });
        }
      });
    }
  }

  ngOnInit(): void { }

}
