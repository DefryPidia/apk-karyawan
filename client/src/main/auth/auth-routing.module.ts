import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { TemplateComponent } from './../app/template/template.component';
import { ReportPreviewComponent } from '../app/report/preview/report-preview.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'preview/:year/:selectedMonth',
        component: ReportPreviewComponent
    },
    {
        path: '',
        component: TemplateComponent,
        loadChildren: () => import('../app/template/template-routing.component').then(mod => mod.TemplateRoutingModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
