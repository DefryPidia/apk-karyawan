// angularcomponents
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// components
import { DeleteComponent } from './delete/delete-confirm.component';
import { LogoutComponent } from './logout/logout-confirm.component';

@NgModule({
    imports: [
        CommonModule
     ],
    declarations: [
        DeleteComponent,
        LogoutComponent
    ],
    exports: [
        DeleteComponent,
        LogoutComponent
    ]
})

export class LibrariesModule { }
