// angularcomponents
import { Component, ViewChild } from '@angular/core';

// components
import { EmployeeListComponent } from '../../main/app/employee/list/employee-list.component';

// services
import { EmployeeListService } from '../../main/app/employee/list/employee-list.service';
import { EmployeeRestService } from '../../main/app/employee/employee-rest.service';
import { AttendanceRestService } from '../../main/app/attendance/attendance-rest.service';
import { AttendanceListService } from '../../main/app/attendance/list/attendance-list.service';

// swal
import Swal from 'sweetalert2';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html'
})
export class DeleteComponent {

  @ViewChild(EmployeeListComponent) empLstC: EmployeeListComponent;
  public dataEmp: any;
  public currentService: string;

  constructor(
    private empLstS: EmployeeListService,
    private empRstS: EmployeeRestService,
    private atdRstS: AttendanceRestService,
    private atdLstS: AttendanceListService,
  ) {
      this.dataEmp = '';
    }

  showConfirm(dataEmp, service): void {
    this.dataEmp = dataEmp;
    this.currentService = service;
    Swal.fire({
      title: '- DELETE CONFIRM -',
      text: 'Are you sure want to delete this?',
      icon: 'warning',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        this.setDelete();
      }
    });
  }

  setDelete(): void {
    if (this.currentService === 'attendance'){
      this.atdRstS.delete(this.dataEmp).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: '- SUCCESS CANCEL ABSENT -',
          showConfirmButton: false,
          timer: 1500
        });
        this.atdLstS.notifyPostDeletion();
      });
    }else{
      this.empRstS.delete(this.dataEmp).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: '- SUCCESS DELETE DATA -',
          showConfirmButton: false,
          timer: 1500
        });
        this.empLstS.notifyPostDeletion();
      });
    }
  }
}
