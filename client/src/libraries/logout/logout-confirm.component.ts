// angularcomponents
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

// ngbcomponents
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-logout-confirm',
  templateUrl: './logout-confirm.component.html'
})
export class LogoutComponent {

  @ViewChild('confirm') confirm: ElementRef;

  public currentModal: NgbModalRef;
  public id: any;

  constructor(
    private router: Router
  ) {
      this.id = '';
  }

  showConfirm(): void {
    Swal.fire({
      title: '- LOG OUT CONFIRM -',
      text: 'Are you sure want to leave this App?',
      icon: 'info',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        this.setLogout();
      }
    });
  }

  closeConfirm(): void {
    if (this.currentModal) {
      this.currentModal.dismiss();
    }
  }

  setLogout(): void {
    localStorage.removeItem('loggedInUser');
    this.router.navigate(['/']);
    this.closeConfirm();
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      }
    });
    Toast.fire({
      icon: 'success',
      title: '- LOG OUT SUCCESSFULY -'
    });
  }

  unsetLogout(): void {
    this.closeConfirm();
    this.id = '';
  }
}
